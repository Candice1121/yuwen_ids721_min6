# Yuwen_IDS721_Mini5: Serverless Rust Microservice

A serverless Rust Lambda function that manages to choose a random song given artist name from the AWS DynamoDB with CloudWatch Logs and X-ray tracing


## :ballot_box_with_check: Requirements
* Add logging to a Rust Lambda function
* Integrate AWS X-Ray tracing
* Connect logs/traces to CloudWatch


## :ballot_box_with_check: Main Progress
### __`Create the Database`__ 

[AWS Developer Guide](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStartedDynamoDB.html)


Step 1: Extend from Mini Project 5: Lambda Function to Read from DynamoDB
1. Update and add dependencies in `Cargo.toml`
    ```
    tracing = "0.1.37"
    tracing-subscriber = "0.2.0"

    ```
2. Modify codes in `src/main.rs` and add following tracing activity in function main
    ```
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("Unable to global default");

    info!("Starting the redactr service...");
    ```

3. Test functionality locally
    ```
    cargo lambda watch
    cargo lambda invoke --data-ascii "{ \"artist\": \"Seventeen\" }"
    ```


4. Build the function to deploy it on AWS Lambda
    ```
    cargo lambda build --release
    cargo lambda deploy
    ```

5. Add policies to role associated with this Lambda Function
    ![policies](imgs/permissions.png)
    
6. Configure X-ray permission under Configuration Settings
    ![configure](imgs/configuration.png)


###  __` Results`__

In the test section under Lambda, create a test event to make sure this function can perform correctly.

Test Result:
![test_result](imgs/test_result.png)

CloudWatch Traces:
![cloud_watch](imgs/cloudwatch.png)

Trace Map:
![trace-map](imgs/trace_map.png)

